// hoisting
// mengakses variabel setelahnya
// console.log(name);
// var name = 2;
// let umur = 19;
// console.log(umur);


// percobaan 1
// var name = "Aku cinta TSI";
// if (true)
// {	
// 	var b = 6;
// 	let c = 7;
// 	console.log(name);
// }

// function test ()
// {	
// 	var a = 4;
// 	console.log(name + " Sangat banget");
// 	console.log(a);
// }
// console.log(b);
// console.log(c);

// test();


// const a = 5;
// a = "kontol";
// console.log(a);



// percobaan 2
// let firstname = "Patcia";
// let lastname = "Isabelle";

// let name = firstname + " " + lastname;

// console.log(name);
// console.log(`hello ${firstname} ${lastname}`);





// percobaan 3
// destructuring

// const person = {
// 	name: "Adik",
// 	age: 28,

// }

// const { name, age} = person;
// console.log("Nama: " + name); 
// console.log('Umur: ' + age);

// // console.log("Nama: " + person.name); 
// // console.log('Umur: ' + person.age);



// const arr = [1,2,3];

// const [a,b,c] = arr;
// console.log(a);
// console.log(b);
// console.log(c);




// percobaan 4
// default parameter

// function declaration
// function add (a = 2, b =3)
// {
// 	return a + b;
// }
// console.log(add());


// function expression
// const minus = function(a, b)
// {
// 	let min = a - b;
// 	return min;
// }


// arrow function
// const add = (a,b) => {

// 	let sum = a + b;
// 	return sum;

// }
// console.log(add(4,2));


// const add = a => a;

// console.log(add("kintil"));


// const add = (a, b) => `My var is ${a} and ${b}`;

// console.log(add("kintil","Hilih"));





