// class dan subclass

class person{


	constructor(name, age)
	{

		this.name = name;
		this.age = age; 

	}

	halo()
	{
		return "Halo " + this.name;
	}
}
class member extends person {

	constructor(name, age, id)
	{
		super(name,age);
		this.id = id;
	}

}


// const tol = new person('Mori', 19);
// const kon = new member('Mori', 19, 666);

// console.log(kon.id);


// spread operator dan rest parameter
// gabung aray
// let arr1 = [1,2,3];
// let arr2 = [...arr1,4,5,6,...arr1];
// console.log(arr2);

// untuk object
// const country = {
// 	id: 1,
// 	negara: "inggris"
// }


// const club = {
// 	...country,
// 	name: "Chelsea"
// }


// console.log(club);


// rest parameter
// function add (a,b, ...rest)
// {
// 	console.log("Restnya ditampung dalam array: " + rest);
// 	let total = 0;
// 	for(var i = 0; i < rest.length; i++){
// 		total += rest[i];
// 	}
// 	return a + b + total;
// }


// console.log(add(5,7,6,6,6));






