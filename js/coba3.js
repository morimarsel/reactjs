// splice method
// const num = [1,2,3];

// num.splice(1,2, 4,5);

// console.log(num);



// ternary
// let bool = true;

// bool ? console.log('true') : console.log('false');

// if (bool)
// {
// 	console.log('ok');
// }

// dapat dipersingakat dengan

// bool && console.log('True');





// higher order func
// map
const fruits = [
	{
		id:1,
		name: "apple",
		price: 2000
	},
	{
		id: 2,
		name: "Banana",
		price: 3600

	},
	{
		id: 3,
		name: "Durian",
		price: 10000

	},
	{
		id:4,
		name: "Durian",
		price: 2500
	}
]


// const fruitname = fruits.map(item => item.name);
// const fruitname = fruits.map(function(item){

// 	return item.name;
// });
// console.log(fruitname);





// filter
// const apple = fruits.filter(item => item.name == "Durian");

// console.log(apple);




// find
// const banana = fruits.find(item => item.name == "Banana");
// console.log(banana)



// reduce
// const totalPrice = fruits.reduce((total, current) => total + current.price, 0 );

// console.log(totalPrice);





// generator atau spesial function
// function biasa(){
// 	return 1;
// }

// console.log(biasa());



function* generator () {
	yield 1;
	yield 2;
	yield 3;
	return 4;

}

// console.log(generator().next().value );
const gen = generator();
console.log(gen.next().value);
console.log(gen.next().value);
console.log(gen.next().value);
console.log(gen.next().done);