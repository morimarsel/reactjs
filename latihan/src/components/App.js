import React from "react"
import List from "./List"
import Button from "./Button"
import "./../styles/App.css"




class App extends React.Component {
	state = {

		song: {
			name: "Sweet Child O Mine",
			author: "Axl Rose"	
		},
		skills: ["Smart","Stupid"],
		name: "Mori",
		text: ""

	}

	change = e => {
		this.setState({
			text: e.target.value
		})
	}

	click = () => {
		this.setState({
			name: this.state.text
		})
		this.setState({
			text: ""
		})
	}

	render(){
		return(
				//ini yg disebut jsx
				<div style = {{width: 'auto', backgroundColor: 'blue'}}>	
					<h1 style = { style }>Hello World { this.state.name }</h1>
					<input type="text" value= {this.state.text} onChange ={this.change}/>
					<button onClick={this.click} > input </button>

					<h3 className="box">Judul: { this.state.song.name }</h3>
					<h3>Author: { this.state.song.author }</h3>
					<h3>Skill: { this.state.skills.map(item => 
						<ol>
  							<li> {item} </li>
						</ol>) }
					</h3>
					<List name="Free Bird" test="Lynrd" 
					judul={ this.state.song.name } author = { this.state.song.author } 
					skill = {this.state.skills.map(item =>  item) }>
						Ini List 1234 asds
					</List>
					<Button color="merah" ></Button>
				</div>

			)
	}
}

const style = {

	color: "white",
	fontSize: '200px'


}



export default App;



// component menggunakan function
// const App = () => {

// 	return(

// 		<div>
// 			<h1>Hello World</h1>
// 		</div>

// 	)
// }


