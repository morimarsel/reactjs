import React from "react"
import propTypes from "prop-types"


class Button extends React.Component{
	render(){
		return(
			
			<div>

				<button>Click</button>
			</div>

		)
	}
}


Button.propTypes = {

	color: propTypes.string.isRequired

}

Button.defaultProps = {
	color: "Biru"
}

export default Button;