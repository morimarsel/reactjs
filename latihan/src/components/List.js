import React from "react"
import ListItem from "./ListItem"

class List extends React.Component{
	render(){
		const data = this.props.skill 
		return(

			<div>
				
				<ListItem/>
				<h1> List Component </h1>
				<p> {this.props.name} </p>
				<p> {this.props.test} </p>
				<p> {this.props.judul} </p>
				<p> {this.props.children} </p>
				
				<h4>
					{data. map(function(d, idx){
						return (<li key={idx}>
									{d}
								</li>)
					})}
					
				</h4>

			</div>

		)
	}

}

export default List;
