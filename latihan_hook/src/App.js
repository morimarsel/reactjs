import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';

const App = () => {
  const [angka, setAngka]= useState(0);
  const [text, setText]= useState("");
  const [array, setArray]= useState([
  {
    id: 1,
    title:"Title"
  }
  ]);
  const [obj, setObj] = useState({
    name:"HELP"
  })

  const add = () => {
    setAngka(angka + 1)
  }

  const kurang = () => {
    setAngka(angka - 1)
  }

  return (
    <div className="App">
      {angka}  
      <button onClick={add}>Tambah</button>
      <button onClick={kurang}>Kurang</button>
    </div>
  );
}

export default App;
