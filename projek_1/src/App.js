import React from "react"
import FormInput from "./components/FormInput"
import TodoItem from "./components/TodoItem"
import EditModal from "./components/EditModal"
import logo from  './logo.svg';
import './App.css';

class App extends React.Component{
  state={
    data:
    [
      {
        id:1,
        title:"Finding God"
      },
      {
        id:2,
        title:"workout training"
      },
      {
        id:3,
        title:"Mori The Great"
      }
    ],
    isEdit: false,

    editData: {
      id: "",
      title: ""
    }
  }

  update = () =>{
    const{id, title } = this.state.editData
    const newData = {id, title}
    const newDatas = this.state.data
    newDatas.splice((id-1), 1, newData)
    this.setState({
      data: newDatas,
      isEdit: false,
      editData: {
        id: "",
        title: ""
      }
    })
  }

  setTitle = e => {
    this.setState({
      editData: {
        ...this.state.editData,
        title: e.target.value
      }
    })
  }

  openModal = (id, data) => {
    this.setState({
      isEdit: true,
      editData: {
        id,
        title: data
      }
    })

  }

  closeModal = () => {
    this.setState({
      isEdit: false
    })
  }

  deleteTask = id => {
    
    if (window.confirm("Sudah Yakin?")) {
      this.setState({
        data: this.state.data.filter(item => item.id != id)
      })  
    }else{
      return null;
    }
    

  }


  addTask = data => {

    const id = this.state.data.length
    const newData= {
      id: id + 1,
      title: data
    }
    this.setState({
      data: [...this.state.data, newData]
    })
  }

  render(){

    const { data } = this.state;
    return(
      
      <div className="app">
        <div className="logo">
          <img src= {logo} alt="logo"/>
          <h3>
            Project 1
          </h3>
        </div>

        <div className="list">
          {data.map( item =>
            <TodoItem 
              key={item.id}
              datum={item} 
              del={this.deleteTask}
              open={this.openModal}
            />
          )}
        </div>

        <div className="input-form">
          <FormInput add={this.addTask} />
        </div>

        <EditModal 
          edit={this.state.isEdit} 
          close={this.closeModal} 
          change={this.setTitle} 
          data={this.state.editData}
          update={this.update}
        />

      </div>
    )
  }
}


export default App;
