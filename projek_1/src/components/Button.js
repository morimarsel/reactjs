import React from "react"
import "../styles/Button.css"
import propTypes from "prop-types"


const Button = props => {

	const delById = id => {

		props.action(id)
	}

 	return(


		<button className={'btn btn-' + props.variant } onClick={() => delById(props.id) } >
			{props.text}
		</button>

 	)

}



Button.propTypes = {
	text: propTypes.string.isRequired,
	variant: propTypes.string.isRequired,
	action: propTypes.func.isRequired
}



export default Button;