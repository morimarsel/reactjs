import React from "react"
import PropTypes from "prop-types"
import Button from "./Button"

import "../styles/FormInput.css"
class FormInput extends React.Component{

	state = {

		text: ""

	}

	change = e => {

		this.setState({ text: e.target.value })
	}

	submit = e => {
		if (this.state.text !== "") {
			this.props.add(this.state.text)
		}

		this.setState({
			text:""
		})
	}

	render(){

		return(

			<div style={inputForm}>
				<input 
					type="text"
					style={input}
					placeholder="Input Di sini"
					onChange={this.change}
					value={this.state.text}
				/>
				<Button text="add" variant="primary" action={this.submit}/>
			</div>
		
		)

	}

}



export default FormInput

FormInput.propTypes = {
	add: PropTypes.func.isRequired
}

const inputForm = 
{
	background: 'white',
	color: 'white',
	display: 'flex',
	alignItems: 'center',
	height: '3rem',
	padding: '0 1rem',
	justifyContent: 'space-between',
	margin: '0.5rem 0'


}


const input = 
{

	width: '70%',

}