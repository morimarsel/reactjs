import React from "react"
import PropTypes from "prop-types"
import Button from "./Button"



const TodoItem = props => {



	const del2 = id => {

    	props.del(id);

  	}

	return(

		<div style = {todoItem}>

			<p>{props.datum.title}</p>
			<div>

				<Button
					text="Edit" 
					variant="success" 
					action={() => props.open(props.datum.id, props.datum.title)}
				 />	
				<Button id={props.datum.id} text="Delete" variant="danger" action={del2} />

			</div>

		</div>

	)

}


TodoItem.propTypes = {
	datum: PropTypes.object.isRequired,
	del: PropTypes.func.isRequired
}


const todoItem = {
	background: '#2da4f8',
	color: 'white',
	display: 'flex',
	alignItems: 'center',
	height: '3rem',
	padding: '0 1rem',
	justifyContent: 'space-between',
	margin: '0.5rem 0'


}


export default TodoItem;