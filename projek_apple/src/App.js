import React from "react";
import Footer from "./components/Footer";
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <Footer/>
      <img src={logo} className="App-logo" alt="logo" />

        
    </div>
  );
}

export default App;
